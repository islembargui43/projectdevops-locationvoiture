<?php


use App\Entity\Client;
use PHPUnit\Framework\TestCase;

class UnitTest extends TestCase
{
    public function testClient(): void
    {
        $client = new Client();
        $client->setCin('12345678');
        $client->setNom('Doe');
        $client->setPrenom('John');
        $client->setAdresse('rue de la paix');
        $this->assertSame('12345678', $client->getCin());
        $this->assertSame('Doe', $client->getNom());
        $this->assertSame('John', $client->getPrenom());
        $this->assertSame('rue de la paix', $client->getAdresse());
    }
}
